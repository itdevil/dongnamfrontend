/**
 * http://usejsdoc.org/
 */

var mainApplicationModuleName = 'dongnam';

var mainApplicationModule = angular.module(mainApplicationModuleName, [
		'ngRoute', 'danhmuc', 'nghiepvu', 'baocao', 'user' ]);

mainApplicationModule.config([ '$locationProvider',
		function($locationProvider) {

			$locationProvider.hashPrefix('!');
		} ]);

angular.element(document).ready(function() {
	angular.bootstrap(document, [ mainApplicationModuleName ]);
	setTimeout(function() {
		$('.selectpicker').selectpicker();
	}, 1000);
});

mainApplicationModule.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

mainApplicationModule.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
       var fd = new FormData();
       fd.append('file', file);
    
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
    
       .success(function(){
       })
    
       .error(function(){
       });
    }
 }]);
