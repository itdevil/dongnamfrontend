/**
 * http://usejsdoc.org/
 */
angular.module('danhmuc').config([ '$routeProvider', function($routeProvider) {

	$routeProvider

	// route for the home page
	.when('/doitac', {
		templateUrl : 'app/danhmuc/views/doitac.html'
	})

	// route for the about page
	.when('/vattuhanghoa', {
		templateUrl : 'app/danhmuc/views/vattuhanghoa.html'
	})

	// route for the contact page
	.when('/khohang', {
		templateUrl : 'app/danhmuc/views/khohang.html'
	})
	
	// route for the contact page
	.when('/login', {
		templateUrl : 'pages/login.html'
	});

	
} ]);