/**
 * http://usejsdoc.org/
 */
angular.module('nghiepvu').config([ '$routeProvider', function($routeProvider) {

	$routeProvider

	// route for the donhang page
	.when('/donhang', {
		templateUrl : 'app/nghiepvu/views/donhang.html'
	})

	// route for the nhap xuat kho page
	.when('/nhapxuatkho', {
		templateUrl : 'app/nghiepvu/views/nhapxuatkho.html'
	})

	// route for the contact page
	.when('/nhaphoadon', {
		templateUrl : 'app/nghiepvu/views/nhaphoadon.html'
	});
	
} ]);