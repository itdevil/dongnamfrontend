/**
 * http://usejsdoc.org/
 */
angular.module('user').config([ '$routeProvider', function($routeProvider) {

	$routeProvider

	// route for the donhang page
	.when('/login', {
		templateUrl : 'app/user/views/login.html'
	});
	
} ]);